/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file <stats.c> 
 * @brief stats implmentation performing various statiscal functions
 *        and first week introduction-embedded-systems course assignment
 *
 * <Add Extended Description Here>
 *
 * @author ramy ezzat
 * @date 07/12/2021
 *
 */



#include <stdio.h>
#include "stats.h"

/* Size of the Data Set */
#define SIZE (40)

/* implmentation internally defined function(s) */
void swap(unsigned char *, unsigned char *);

void main() {

  unsigned char test[SIZE] = { 34, 201, 190, 154,   8, 194,   2,   6,
                              114, 88,   45,  76, 123,  87,  25,  23,
                              200, 122, 150, 90,   92,  87, 177, 244,
                              201,   6,  12,  60,   8,   2,   5,  67,
                                7,  87, 250, 230,  99,   3, 100,  90};

  /* Other Variable Declarations Go Here */
  /* Statistics and Printing Functions Go Here */
  print_array(test, SIZE);
  print_statistics(test, SIZE);
  sort_array(test, SIZE, 'd');
  print_array(test, SIZE);
}

/* Add other Implementation File Code Here | functions defined in the header*/

void print_statistics(unsigned char * _array, unsigned int _length){
  /* @brief  function that prints the statistics of an array including minimum, maximum, mean, and median
     @param _array unsigned char pointer or an array address
     @param _length unsigned int array length
     @return null
  */
  printf("min number in the array: %d\n", find_min(_array, _length));
  printf("max number in the array: %d\n", find_max(_array, _length));
  printf("array mean: %d\n", (int) find_mean(_array, _length));
  printf("array median: %d\n", (int) find_median(_array, _length));
}

void print_array(unsigned char * _array, unsigned int _length){
  /* @brief prints the array to the screen
     @param _array unsigned char pointer or an array address
     @param _length unsigned int array length
     @return null
  */
  printf("array: \n\t");
  for(int i = 0; i < _length; i++){
    if (i == _length - 1)
      printf("%u", _array[i]); // no space for last element
    else
      printf("%u, ", _array[i]); // elements in the middle
    if (((i + 1) % 10 == 0) && (i + 1 != SIZE))
      printf("\n\t"); // first elemnts
  }
  printf("\n"); // last endline
}

float find_median(unsigned char * _array, unsigned int _length){
  /* @brief calculates median value
     @param _array unsigned char pointer or an array address
     @param _length unsigned int array length
     @return float median value
  */
  float _res;

  sort_array(_array, SIZE, 'a'); // sort array in an ascending order
  if(_length % 2 == 0) // even array length | average of the 2 middle elements
      _res = (_array[(_length - 1) / 2] + _array[_length / 2]) / 2.0;
  else //odd array length | assign middle element
      _res = _array[_length / 2];
  return _res;
}

float find_mean(unsigned char * _array, unsigned int _length){
  /* @brief calculates mean value
     @param _array unsigned char pointer or an array address
     @param _length unsigned int array length
     @return float mean value
  */
  float _sum_res, _res;

  for(int i = 0; i < _length; i++)
    _sum_res += _array[i]; // loop over array elements and summing them
  _res = _sum_res / _length; 
  return _res;
}


unsigned char find_max(unsigned char * _array, unsigned int _length){
  /* @brief finds an array maximum element
     @param _array unsigned char pointer or an array address
     @param _length unsigned int array length
     @return unsigned char value of maximum array elemnet
  */
  unsigned char _res, _item;

  for (int i = 0; i <_length; i++){ // loop over the array
    _item = _array[i]; // assign temporary item
    if(i == 0)
      _res = _item;
    if(_item > _res)
      _res = _item;
    else
      continue;
  }
  return _res;
}

unsigned char find_min(unsigned char * _array, unsigned int _length){
  /* @brief inds an array minimum element
     @param _array unsigned char pointer or an array address
     @param _length unsigned int array length
     @return unsigned char value of minimum array elemnet
  */
  unsigned char _res;
  unsigned char _item;
  for (int i = 0; i <_length; i++){
    _item = _array[i];
    if(i == 0)
      _res = _item;
    if(_item < _res)
      _res = _item;
    else
      continue;
  }
  return _res;
}

void sort_array(unsigned char * _array, unsigned int _length, char _order){
  /* @brief sorts an unsigned char array
     @param _array unsigned char pointer or an array address
     @param _length unsigned int array length
     @param _order char 'a' ascending and 'd' descending, with 'a' as defult
            if neither was found
     @return null
  */
  int i, j, element;

  if (_order != 'a' && _order != 'd') // assign default
    _order = 'a';
 
  for (i = 0; i < _length - 1; i++) {
      element = i; // find the max/min element in unsorted array and assign if met sorting criteria
      for (j = i + 1; j < _length; j++)
          if ((_order == 'd' && _array[j] > _array[element]) || (_order == 'a' && _array[j] < _array[element]))
              element = j;
      swap(&_array[element], &_array[i]); // swap the found element with the first element
  }
}


void swap(unsigned char * x, unsigned char * y){
  /* @brief swaps elements values within a memory address
            can be used to swap unsigned char array elements
     @param x unsigned char first element to be swaped
     @param y unsigned char second element to be swaped
     @return null
  */
  unsigned char temp = *x;
  *x = *y;
  *y = temp;
}
